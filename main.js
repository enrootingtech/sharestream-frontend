"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
var win, serve;
var args = process.argv.slice(1);
var currentWidth = 0, currentHeight = 0, positionY = 0, positionX = 0;
serve = args.some(function (val) { return val === '--serve'; });
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
// var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
//   // Someone tried to run a second instance, we should focus our window.
//   if (win) {
//     if (win.isMinimized()) win.restore();
//     win.focus();
//   }
// });
// if (shouldQuit) {
//   app.quit();
//   process.exit();
// }
function createWindow() {
    var electronScreen = electron_1.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;
    var iconUrl = (serve) ? 'src' : 'dist';
    // Create the browser window.
    console.log(path.join('./', iconUrl + '/favicon.ico'), path.join(__dirname, iconUrl + '/favicon.ico'));
    win = new electron_1.BrowserWindow({
        center: true,
        width: size.width - 100,
        height: size.height - 100,
        minWidth: 980,
        minHeight: 648,
        backgroundColor: "#f7f7f7",
        frame: false,
        icon: "file:///" + path.join(__dirname, iconUrl + '/favicon.ico'),
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true
        }
    });
    if (serve) {
        require('electron-reload')(__dirname, {
            electron: require(__dirname + "/node_modules/electron")
        });
        win.loadURL('http://localhost:4200/');
        win.webContents.openDevTools();
    }
    else {
        win.loadURL(url.format({
            pathname: path.join(__dirname, 'dist/index.html'),
            protocol: 'file:',
            slashes: true
        }));
        win.webContents.openDevTools();
    }
    // Emitted when the window is closed.
    win.on('closed', function () {
        // Dereference the window object, usually you would store window
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
electron_1.app.on('ready', createWindow);
// Quit when all windows are closed.
electron_1.app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
electron_1.app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow();
    }
});
electron_1.ipcMain.on("minimize", function () {
    if (win !== null) {
        win.minimize();
    }
});
electron_1.ipcMain.on("fullscreen", function () {
    if (win !== null) {
        win.setFullScreen(!win.isFullScreen());
    }
});
electron_1.ipcMain.on("maximize", function () {
    if (win !== null) {
        var electronScreen = electron_1.screen;
        var size = electronScreen.getPrimaryDisplay().workAreaSize;
        var sizes = win.getSize();
        var pos = win.getPosition();
        positionY = pos[1];
        positionX = pos[0];
        currentWidth = sizes[0];
        currentHeight = sizes[1];
        win.setSize(size.width, size.height, true);
        win.center();
    }
});
electron_1.ipcMain.on("restore", function () {
    if (win !== null) {
        win.setSize(currentWidth, currentHeight, true);
        win.setPosition(positionX, positionY);
    }
});
electron_1.ipcMain.on("close", function () {
    if (win !== null) {
        win.close();
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBK0Q7QUFDL0QsMkJBQTZCO0FBQzdCLHlCQUEyQjtBQUUzQixJQUFJLEdBQWtCLEVBQUUsS0FBSyxDQUFDO0FBQzlCLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ25DLElBQUksWUFBWSxHQUFHLENBQUMsRUFBRSxhQUFhLEdBQUcsQ0FBQyxFQUFFLFNBQVMsR0FBRyxDQUFDLEVBQUUsU0FBUyxHQUFHLENBQUMsQ0FBQztBQUN0RSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsS0FBSyxTQUFTLEVBQWpCLENBQWlCLENBQUMsQ0FBQztBQUU1QyxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLEdBQUcsTUFBTSxDQUFBO0FBRTFELG9GQUFvRjtBQUNwRiwyRUFBMkU7QUFDM0UsZUFBZTtBQUNmLDRDQUE0QztBQUM1QyxtQkFBbUI7QUFDbkIsTUFBTTtBQUNOLE1BQU07QUFFTixvQkFBb0I7QUFDcEIsZ0JBQWdCO0FBQ2hCLG9CQUFvQjtBQUNwQixJQUFJO0FBR0osU0FBUyxZQUFZO0lBRW5CLElBQU0sY0FBYyxHQUFHLGlCQUFNLENBQUM7SUFDOUIsSUFBTSxJQUFJLEdBQUcsY0FBYyxDQUFDLGlCQUFpQixFQUFFLENBQUMsWUFBWSxDQUFDO0lBQzdELElBQU0sT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO0lBQ3pDLDZCQUE2QjtJQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sR0FBRyxjQUFjLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQTtJQUN0RyxHQUFHLEdBQUcsSUFBSSx3QkFBYSxDQUFDO1FBQ3RCLE1BQU0sRUFBRSxJQUFJO1FBQ1osS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRztRQUN2QixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHO1FBQ3pCLFFBQVEsRUFBRSxHQUFHO1FBQ2IsU0FBUyxFQUFFLEdBQUc7UUFDZCxlQUFlLEVBQUUsU0FBUztRQUMxQixLQUFLLEVBQUUsS0FBSztRQUNaLElBQUksRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxHQUFHLGNBQWMsQ0FBQztRQUNqRSxjQUFjLEVBQUU7WUFDZCxlQUFlLEVBQUUsSUFBSTtZQUNyQixVQUFVLEVBQUUsSUFBSTtTQUNqQjtLQUNGLENBQUMsQ0FBQztJQUVILElBQUksS0FBSyxFQUFFO1FBQ1QsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUMsU0FBUyxFQUFFO1lBQ3BDLFFBQVEsRUFBRSxPQUFPLENBQUksU0FBUywyQkFBd0IsQ0FBQztTQUN4RCxDQUFDLENBQUM7UUFDSCxHQUFHLENBQUMsT0FBTyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdEMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztLQUNoQztTQUFNO1FBQ0wsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO1lBQ3JCLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxpQkFBaUIsQ0FBQztZQUNqRCxRQUFRLEVBQUUsT0FBTztZQUNqQixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUMsQ0FBQyxDQUFDO1FBQ0osR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztLQUNoQztJQUlELHFDQUFxQztJQUNyQyxHQUFHLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRTtRQUNmLGdFQUFnRTtRQUNoRSxtRUFBbUU7UUFDbkUsb0RBQW9EO1FBQ3BELEdBQUcsR0FBRyxJQUFJLENBQUM7SUFDYixDQUFDLENBQUMsQ0FBQztBQUVMLENBQUM7QUFHRCx3REFBd0Q7QUFDeEQseURBQXlEO0FBQ3pELHNEQUFzRDtBQUN0RCxjQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztBQUU5QixvQ0FBb0M7QUFDcEMsY0FBRyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRTtJQUMxQiwyREFBMkQ7SUFDM0QsOERBQThEO0lBQzlELElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7UUFDakMsY0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO0tBQ1o7QUFDSCxDQUFDLENBQUMsQ0FBQztBQUVILGNBQUcsQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFO0lBQ2pCLGdFQUFnRTtJQUNoRSw0REFBNEQ7SUFDNUQsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO1FBQ2hCLFlBQVksRUFBRSxDQUFDO0tBQ2hCO0FBQ0gsQ0FBQyxDQUFDLENBQUM7QUFHSCxrQkFBTyxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUU7SUFDckIsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO1FBQ2hCLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtLQUNmO0FBQ0gsQ0FBQyxDQUFDLENBQUE7QUFDRixrQkFBTyxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUU7SUFDdkIsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO1FBQ2hCLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQTtLQUN2QztBQUNILENBQUMsQ0FBQyxDQUFBO0FBQ0Ysa0JBQU8sQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFO0lBQ3JCLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtRQUNoQixJQUFNLGNBQWMsR0FBRyxpQkFBTSxDQUFDO1FBQzlCLElBQU0sSUFBSSxHQUFHLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFlBQVksQ0FBQztRQUM3RCxJQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDNUIsSUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzlCLFNBQVMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDbEIsU0FBUyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNsQixZQUFZLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLGFBQWEsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDMUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO0tBQ2Q7QUFDSCxDQUFDLENBQUMsQ0FBQTtBQUNGLGtCQUFPLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRTtJQUNwQixJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7UUFDaEIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQzlDLEdBQUcsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFBO0tBQ3RDO0FBQ0gsQ0FBQyxDQUFDLENBQUE7QUFDRixrQkFBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUU7SUFDbEIsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO1FBQ2hCLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtLQUNaO0FBQ0gsQ0FBQyxDQUFDLENBQUEifQ==