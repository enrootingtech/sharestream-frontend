import * as path from 'path';

export const environment = {
  production: true,
  apiUrl: 'http://192.168.43.212:8000/api/',
  ws: 'ws:/192.168.43.212:8090',
  basePath: (_path: string): string =>{
    _path = "file:///" + path.resolve("./resources/app/dist/" + _path.replace(/\..\//g, ''));
    return _path;
  },
  base_path: (_path: string): string =>{
    _path = path.resolve("./resources/app/dist/" + _path.replace(/\..\//g, ''));
    return _path;
  },
  filepath: (_path: string): string =>{
    return 'file:///' + _path;
  }
};
