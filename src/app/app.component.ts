import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, Event, NavigationEnd, } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { TransportService } from './utility/transport/transport.service';
import { ChatService } from './services/chat/chat.service';
import { Subscription } from 'rxjs';
import { TokenService } from './services/token/token.service';
import { NotifyService } from './services/notify/notify.service';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: '.app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public bucket: Observable<any>;
  private subscriptions: Subscription[] = [];

  constructor(
    private route: Router,
    private chatService: ChatService,
    private token: TokenService,
    private transport: TransportService,
    private notify: NotifyService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.routeChanges()
    this.subscriptions.push(
      this.transport.call.subscribe((message: any) => {
        this.transportSwitch(message)
      })
    )
  }

  ngOnDestroy() {
    console.log("Going offline")
    localStorage.setItem('share-socket', 'offline')
    localStorage.removeItem('shareUserInfo')
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private routeChanges() {
    this.route.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        let main = document.getElementsByClassName('main-app')[0];
        if (main == null) return;
        main.setAttribute('class', '');
        main.classList.add("main-app")
        this.scrollTop()
        switch (event.url) {
          case "/":
          case "/register":
          case "/login":
            main.classList.add("welcome-bg")
            break;
          default:
            main.classList.add('app-theme')
            break;
        }

      }

    });
  }

  private scrollTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  private activateAPP() {
    this.subscriptions.push(
      this.chatService.messages.subscribe((message: any) => {
        let data = JSON.parse(message.data)
        this.dataSwitch(data)
      })
    )
  }

  private transportSwitch(data: any) {
    if (data.type == "socket-subscribe") {
      console.log("Signal to subscribe to socket")
      this.activateAPP()
    }
  }

  private dataSwitch(data: any) {
    if (data.type == "id") {
      console.log("My UID = " + data.id + ", and Send info for websocket")

      let info = {
        id: data.id
      }
      localStorage.setItem('shareUserInfo', JSON.stringify(info))

      let localData = this.token.get('user');
      this.chatService.send({
        type: 'set-info',
        msg: {
          id: localData.identity,
          name: localData.first_name + " " + localData.last_name,
          email: localData.email,
          picture: localData.picture
        }
      })
    } else {
      if (!this.authService.isAuthenticated()) return;
      this.transport.send(data)
    }

    if (data.type == 'broadcast') {
      this.newBroadcast(data.msg)
    }
  }

  private newBroadcast(data: any) {
    this.notify.showNotification('info', "New stream from " + data.email)
    console.log("New broadcast from " + data.email)
  }

}
