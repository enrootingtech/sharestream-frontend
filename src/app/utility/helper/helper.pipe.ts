import { Pipe, PipeTransform } from '@angular/core';
import * as distanceInWordsToNow from 'date-fns/distance_in_words_to_now';

@Pipe({
  name: 'helper'
})
export class HelperPipe implements PipeTransform {

  transform(value: any, type: string, extra?: any): any {
    let result = ''
    switch (type) {
      case 'size':
        result = this.convertSize(value, ((isNaN(extra)) ? 1 : extra))
        break;
      case 'duration':
        result = this.convertToLength(value)
        break;
      case 'toNow':
        result = this.toNow(value);
        break;
    }
    return result;
  }

  private toNow(date: any): string {
    return distanceInWordsToNow(new Date(date), {
      includeSeconds: true,
      addSuffix: true
    });
  }

  private convertSize(bytes: number, decimalPoint: number): string {
    if (bytes == 0) return '0 Bytes';
    let k = 1024,
      dm = decimalPoint || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  private convertToLength(seconds: number) {
    return new Date(seconds * 1000).toISOString().substr(11, 8);
  }

}
