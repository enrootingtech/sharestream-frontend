import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TokenService } from '../services/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class Helpers {

  constructor(private tokenService: TokenService) { }

  public static get Url(): string {
    return environment.apiUrl;
  }

  public static get EXTENTIONS(): string[] {
    return ["vnd.dlna.mpeg-tts", "3g2", "3gpp", "aaf", "asf", "avchd", "avi", "drc", "flv", "m2v", "m4p", "m4v", "x-matroska", "mng", "mov", "mp2", "mp4", "mp3", "mpe", "mpeg", "mpg", "mpv", "mxf", "nsv", "ogg", "ogv", "qt", "rm", "rmvb", "roq", "svi", "vob", "webm", "wmv", "yuv"];
  }

  public static get DEFAULT_CODEC(): any {
    return {
      'webm': 'video/webm; codecs="vp8,vorbis"',
      'mp4': 'video/mp4; codecs="avc1.4D481F, mp4a.40.2"' ,
     };
  }

  public headers() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.tokenService.get('token')
    });
  }

  public arrangeByDate(data: any, newData: any, order: string): any {
    data.push(newData);
    data.sort(function (a, b) {
      if (order == "desc") {
        return parseInt((new Date(b.date).getTime() / 1000).toFixed(0)) - parseInt((new Date(a.date).getTime() / 1000).toFixed(0))
      } else {
        return parseInt((new Date(a.date).getTime() / 1000).toFixed(0)) - parseInt((new Date(b.date).getTime() / 1000).toFixed(0))
      }
    });
    return data;
  }
}
