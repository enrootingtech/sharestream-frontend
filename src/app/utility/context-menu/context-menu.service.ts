import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContextMenuService {

  constructor() { }

  public activate(){
    this.disableMenu()
  }

  private disableMenu() {
    let holder = document.getElementsByClassName('player-holder')[0];
    if (holder == null) return;

    holder.addEventListener("contextmenu", function (e) {
      e.preventDefault();
      console.log("Custom Context Menu")
    }, false);

    holder.addEventListener("keydown", function (e: any) {
      //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (e.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);

    let disabledEvent = function (e: any) {
      if (e.stopPropagation) {
        e.stopPropagation();
      } else if (window.event) {
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }

  }
}
