import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SocketMessage } from '../../interface/socketMessage';

@Injectable({
  providedIn: 'root'
})
export class TransportService {

  private callBehavior = new Subject<SocketMessage>();
  public call = this.callBehavior.asObservable();

  constructor() { }

  send(data: SocketMessage){
  	this.callBehavior.next(data);
  }

}
