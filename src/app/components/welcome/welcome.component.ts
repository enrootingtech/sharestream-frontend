import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: '.welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  public loginActivate: boolean;
  public logo = environment.basePath('../../../assets/img/logo.png');

  constructor(
    private route: Router
  ) {
  }

  ngOnInit() {

    if (window.location.hash == "#/" || window.location.hash == '#/login') {
      this.loginActivate = true;
    } else {
      this.loginActivate = false;
    }

    this.routeChanges()
  }

  private routeChanges() {
    this.route.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        switch (event.url) {
          case "/register":
            this.loginActivate = false;
            break;
          default:
            this.loginActivate = true;
            break;
        }
      }
    });
  }

  changeRoute(state: any) {
    this.loginActivate = state;
  }

}
