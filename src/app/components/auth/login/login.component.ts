import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'src/app/services/notify/notify.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from 'src/app/services/token/token.service';
import { EmailValidator } from '@angular/forms';
import { environment } from '../../../../environments/environment';

@Component({
  selector: '.login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public disableBtn: boolean;
  public checkIcon = environment.basePath('../../../../assets/img/icons/check.png');
  public loginForm: any;

  constructor(
    private notify: NotifyService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private tokenService: TokenService
  ) { }

  ngOnInit() {

    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/', 'home']);
    }

    this.disableBtn = false;
    this.loginForm = {
      email: null,
      password: null
    };
  }

  onSubmit(event: any) {
    this.disableBtn = true;
    let _this = event.target
    this.loginForm.email = _this.querySelector('#share-email').value;
    this.loginForm.password = _this.querySelector('#share-password').value;
    this.authService.login(this.loginForm).subscribe(
      (response) => this.handleResponse(response),
      (error) => this.handleError(error)
    );
  }

  private handleResponse(response: any) {

    let next = this.route.snapshot.queryParamMap.get('next'),
      location = ['/', 'home'];

    if (next != null && next.length != 0) {
      // location = next;
    }
    // this.notify.showNotification('success', 'Successfully logged in');
    this.tokenService.handle(response);
    setTimeout(() => {
      this.router.navigate(location).finally(() => {
        // window.location.reload();
      });
    }, 500);
  }

  private handleError(data) {
    let msg = (data.error.error == undefined) ? "Known error has error, please try again" : data.error.error;
    this.notify.showNotification('error', msg);
    this.disableBtn = false;
    let pass: any = document.querySelector('#share-password');
    pass.value = '';
    console.log(data)
  }

}
