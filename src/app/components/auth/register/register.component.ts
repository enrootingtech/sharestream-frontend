import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import { NotifyService } from 'src/app/services/notify/notify.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: '.register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public disableBtn: boolean;
  public registerForm: any;
  public checkIcon = environment.basePath('../../../../assets/img/icons/check.png');

  constructor(
    private authService: AuthService,
    private router: Router,
    private notify: NotifyService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/', 'home']);
    }

    this.disableBtn = false;
    this.registerForm = {
      first_name: null,
      last_name: null,
      email: null,
      password: null,
      sendInfo: false
    };
  }

  onSubmit(event: any) {
    this.disableBtn = true;
    let _this = event.target
    this.registerForm.first_name = _this.querySelector('#share-firstName').value;
    this.registerForm.last_name = _this.querySelector('#share-lastName').value;
    this.registerForm.email = _this.querySelector('#share-email').value;
    this.registerForm.password = _this.querySelector('#share-password').value;
    this.registerForm.sendInfo = (_this.querySelector('#extra-info:checked') == null) ? false : true;
    this.authService.register(this.registerForm).subscribe(
      (response) => this.handleResponse(response),
      (error) => this.handleError(error)
    );
  }

  private handleResponse(response: any) {

    let location = ['/', 'home'],
      data = response.data,
      session = data.session
      let saveInfo = {
        access_token: session.access_token,
        user: session.user
      };

    this.tokenService.handle(saveInfo);
    setTimeout(() => {

      this.router.navigate(location).finally(() => {
        // window.location.reload();
      });
    }, 500);
  }

  private handleError(data) {

    let msg = (data.error.error == undefined) ? "Known error has error, please try again" : data.error.error;
    this.notify.showNotification('error', msg);
    this.disableBtn = false;
    let pass: any = document.querySelector('#share-password');
    pass.value = '';
    // console.log(data)
  }

}
