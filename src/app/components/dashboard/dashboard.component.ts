import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { TokenService } from 'src/app/services/token/token.service';
import { ChatService } from 'src/app/services/chat/chat.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: '.dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public firstName: string;

  constructor(
    private transport: TransportService,
    private token: TokenService,
    private chatService: ChatService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.firstName = "ShareStream User";
    this.updateUserInfo();
    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.transportHandle(data)
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private transportHandle(data: any) {
    switch (data.type) {

    }
  }

  private updateUserInfo() {
    let localData = this.token.get('user');
    if (localData == null) return;
    this.firstName = localData.first_name;

    this.transport.send({
      type: 'profile-update',
      msg: []
    })

    let connected = false;
    if (localStorage.getItem('share-socket') != null) {
      if (localStorage.getItem('share-socket') == 'online') {
        connected = true;
      }
    }

    if (!connected) {
      console.log("Starting websocket connection")
      this.chatService.connect();
    }

    let waitToConnect = setInterval(() => {
      if (!this.authService.isAuthenticated()) {
        clearInterval(waitToConnect)
        return;
      }
      if (localStorage.getItem('share-socket') == 'online') {
        clearInterval(waitToConnect)
      } else {
        console.log("Reconnecting...")
        this.chatService.connect();
      }
    }, 1000)
  }

}
