import { Component, OnInit, OnDestroy } from '@angular/core';
import { ElectronService } from 'src/app/services/electron/electron.service';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat/chat.service';

@Component({
  selector: '.top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public firstName: string;
  public image: string;
  public loggedIn: boolean;
  public iconPath = environment.basePath('../../../../favicon.ico');

  constructor(
    private electronService: ElectronService,
    private transport: TransportService,
    private authService: AuthService,
    private token: TokenService,
    private router: Router,
    private chat: ChatService
  ) { }

  ngOnInit() {

    this.loggedIn = this.authService.isAuthenticated();
    this.firstName = "Share User";
    this.image = environment.basePath('../../../../assets/img/default.png');
    this.updateTopBar();
    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.transportHandle(data)
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private transportHandle(data: any) {
    switch (data.type) {
      case 'profile-update':
        this.updateTopBar();
        break;
    }
  }

  private updateTopBar() {
    this.loggedIn = this.authService.isAuthenticated();
    let data = this.token.get('user');
    if (data == null) return;

    if (data.first_name != null) {
      this.firstName = data.first_name;
    }

    if (data.picture != '') {
      this.image = data.picture;
    }
  }

  logout(event: any) {
    event.preventDefault();
    this.authService.logout().subscribe(
      data => this.handleLogout(data),
      error => this.handleLogOutError(error)
    );
  }

  handleLogout(data: any) {
    this.token.removeAll();
    this.updateTopBar()
    this.router.navigate(['/']).finally(() => {
      // window.location.reload();
      this.chat.close()
    });
  }

  handleLogOutError(error) {
    console.log(error)
  }

  minimise() {
    if (this.electronService.isElectron()) {
      this.electronService.ipcRenderer.send("minimize")
    }
  }

  maximize() {
    if (this.electronService.isElectron()) {
      let screenView = document.getElementById('maximize');

      if (screenView.classList.contains('window-maximize')) {
        this.electronService.ipcRenderer.send("maximize");
        screenView.classList.remove('window-maximize')
        screenView.classList.add('window-unmaximize')
        screenView.setAttribute('title', 'Restore Down')
      } else {
        this.electronService.ipcRenderer.send("restore");
        screenView.classList.remove('window-unmaximize')
        screenView.classList.add('window-maximize')
        screenView.setAttribute('title', 'Maximize')
      }
    }
  }
  close() {
    if (this.electronService.isElectron()) {
      this.electronService.ipcRenderer.send("close")
    }
  }

}
