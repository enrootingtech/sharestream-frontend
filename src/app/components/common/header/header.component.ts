import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: '.app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logo = environment.basePath('../../../../assets/img/logo.png');

  constructor() { }

  ngOnInit() {
    this.updateStatus();
  }

  private updateStatus() {
    let search = setInterval(() => {
      let status = localStorage.getItem('share-socket'),
        state = 'online';
      if (status == null || status == 'undefined') {
        state = 'offline';
      } else {
        clearInterval(search)
      }
      state = status;
      let elem = document.getElementById('connection-status')
      if (elem != null) {
        if (state == 'online') {
          elem.classList.add('online')
        } else {
          elem.classList.remove('online')
        }
      }
    }, 200)
  }

}
