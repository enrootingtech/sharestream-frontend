import { Component, OnInit } from '@angular/core';
import { MediacontrolService } from 'src/app/services/media-control/media-control.service.js';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  private video: any;
  private file: File;

  constructor(
    private mediaControl: MediacontrolService
  ) { }

  ngOnInit() {
    this.video = document.getElementById('source')
    this.file = null;

  }

  /**
   * Accepting mp4 video file from the user
   */
  onChange(event) {
    let files = event.target.files;
    this.file = files[0];
    let playerData = {
      file: files[0],
      inControl: true,
      endDuration: 55582.25,
      activeVideo: 0,
      roomId: 'kydbd',
      datachannel: null,
      environment: 'test',
      video: this.video
    }

    this.mediaControl.initiate(playerData)

  }



}
