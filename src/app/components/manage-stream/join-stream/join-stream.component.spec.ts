import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinStreamComponent } from './join-stream.component';

describe('JoinStreamComponent', () => {
  let component: JoinStreamComponent;
  let fixture: ComponentFixture<JoinStreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinStreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
