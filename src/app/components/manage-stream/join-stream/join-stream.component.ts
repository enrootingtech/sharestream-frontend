import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { Subscription } from 'rxjs';
import { TokenService } from 'src/app/services/token/token.service';
import { RequestService } from 'src/app/services/request/request.service';
import { Helpers } from 'src/app/utility/helpers';

@Component({
  selector: '.join-stream',
  templateUrl: './join-stream.component.html',
  styleUrls: ['./join-stream.component.scss']
})
export class JoinStreamComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public streams: any;

  constructor(
    private transport: TransportService,
    private token: TokenService,
    private httpRequest: RequestService,
    private helper: Helpers
  ) { }

  ngOnInit() {

    this.streams = [];

    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.dataSwitch(data)
      })
    )

    this.fetchStreams();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private dataSwitch(data: any) {
    switch (data.type) {
      case 'broadcast':
        this.receiveNotification(data)
        break;
      case 'kill-stream':
        this.killStream(data);
        break;
      default:
        break;
    }
  }

  private fetchStreams() {
    this.httpRequest.get('account/' + this.token.getUser('user_id') + '/notifications', { state: "unread", type: "stream" })
      .subscribe((data: any) => {
        data = data.data;
        if (data.length != 0) {
          for (let i = 0; i < data.length; i++) {
            let message = data[i].message;
            let newData = {
              email: message.email,
              roomId: message.roomId,
              title: message.title,
              image: message.image,
              media: message.media,
              date: message.created_at
            }
            this.streams = this.helper.arrangeByDate(this.streams, newData, 'desc');
          }

        }

        // console.log(data, this.streams)
      },
        (error: any) => {
          console.error("Fetch Stream Error: ", error)
        });
  }

  private killStream(data: any) {
    console.log("Kill stream signal: ", data, this.streams)
    if (this.streams.length == 0) return;
    for (let i = 0; i < this.streams.length; i++) {
      if (this.streams[i].roomId == data.msg.roomId && this.streams[i].email == data.msg.email) {
        this.streams.splice(i, 1)
      }
    }
  }

  private receiveNotification(data: any) {
    data = data.msg;

    let input = {
      media: data.media,
      title: data.title,
      image: data.image,
      email: data.email,
      roomId: data.roomId,
      date: data.created_at
    };
    if (input.media.length == 0) return;
    let newArrangedData = this.helper.arrangeByDate(this.streams, input, 'desc');
    this.streams = newArrangedData;
    console.log("Recieved notification: ", this.streams)
  }

  // Join Stream, upon join, create peerConnection and use the Offer(SDP) to create an answer and send it back to the Host(stream provider)

  public joinStream(index: number) {
    console.log("Join: ", this.streams[index]);
    let stream = this.streams[index],
      userInfo = this.token.get('user');

    // Send signal to create peer connection
    let data = {
      type: 'join-request',
      msg: {
        to: stream.email,
        from: userInfo.email,
        roomId: stream.roomId,
        bucket: stream.media
      }
    };
    this.transport.send(data)
  }

}
