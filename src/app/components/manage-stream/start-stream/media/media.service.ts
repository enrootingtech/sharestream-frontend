import { Injectable } from '@angular/core';
import { ElectronService } from 'src/app/services/electron/electron.service';
import { environment } from '../../../../../environments/environment';
import { NotifyService } from 'src/app/services/notify/notify.service';
import { TransportService } from 'src/app/utility/transport/transport.service';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(
    private electron: ElectronService,
    private notify: NotifyService,
    private transport: TransportService
  ) { }

  /**
   * Fragmenting video file to mp4
   * @param event
   */
  public handleRequest(data: any) {
    let file = data.file,
      index = data.index,
      home = process.env.HOMEDRIVE + process.env.HOMEPATH,
      path = this.electron.path,
      fs = this.electron.fs,
      childProcess = this.electron.childProcess;

    let elem = document.querySelector('.item-' + index + ' .item-image');
    elem.classList.add('processing')

    // Create a directory to store fragmented files if it doesn't exist already
    let fragmentedFilesDirectory = path.join(home, '.sharestream')
    if (!fs.existsSync(fragmentedFilesDirectory)) {
      fs.mkdirSync(fragmentedFilesDirectory)
    }

    // The final file must be a mp4
    let destinationFile = file.name.substr(0, file.name.lastIndexOf('.')) + '.mp4'
    let destinationPath = path.join(fragmentedFilesDirectory, destinationFile)

    // The folder where the mp4 tools' binaries are stored
    let binPath = environment.production ? environment.base_path('assets/bin') : 'src/assets/bin',
      mp4fragmentPath = 'mp4fragment.exe'

    let fileExtension = file.name.toLowerCase().split('.').pop()
    if (fileExtension === 'mp4') {
      // Check if a fragmented version of the selected already exists
      if (!fs.existsSync(destinationPath)) {
        // mp4fragment input.mp4 .netsix/output.mp4
        let work = childProcess.spawn(path.join(binPath, mp4fragmentPath), [file.path, destinationPath]);

        work.on('error', (error: any) => {
          console.error(`error: ${error}`)
          this.notify.showNotification('error', 'An error occurred during the file fragmentation process.')
        })

        work.stderr.on('data', (stderr: any) => {
          console.log(`stderr: ${stderr}`)
        })

        work.stderr.on('end', () => {
          // Success, get file information to instantiate the MediaSource object
          this.emitInfo(index, destinationPath)
        })

        work.stdout.on('data', (stdout) => {
          console.log(`stdout: ${stdout}`)
        })

        this.watcher(destinationPath, work)

      } else {
        // A fragmented version of the selected file already exists
        // No need to fragment again, just get the file information

        setTimeout(() => {
          this.emitInfo(index, destinationPath)
        }, 500)
      }
    } else {
      // handleGetMkvRequest(commit, selectedFile)
    }

  }

  private watcher(path: string, work: any) {
    // This is the fs watch function.
    this.electron.fs.watchFile(path, (curr, prev) => {
      console.log(curr, prev)
    });
  }

  private emitInfo(index: number, newPath: string) {
    this.transport.send({
      type: 'fragment-done',
      msg: {
        path: newPath,
        index: index
      }
    })
  }

  public removeFile(path: string) {
    let fileDiretory = path;
    console.log(fileDiretory.includes('.sharestream'))
    if (fileDiretory.includes('.sharestream')) {
      if (this.electron.fs.existsSync(fileDiretory)) {
        this.electron.fs.unlinkSync(fileDiretory)
      }
    }
  }

}
