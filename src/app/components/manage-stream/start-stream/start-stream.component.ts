import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FriendService } from 'src/app/services/friend/friend.service';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { Helpers } from '../../../utility/helpers';
import { NotifyService } from 'src/app/services/notify/notify.service';
import { environment } from '../../../../environments/environment';
import { MediaService } from './media/media.service';

@Component({
  selector: '.start-stream',
  templateUrl: './start-stream.component.html',
  styleUrls: ['./start-stream.component.scss']
})
export class StartStreamComponent implements OnInit, OnDestroy {

  @ViewChild('fileReader', { static: true }) fileReader: ModalDirective;

  public data_type = 'media';
  public bucket: any = [];
  public audience: any = []
  public sendTo: any = []
  private subscriptions: Subscription[] = [];
  private file: File;
  private circumference: number;
  public audienceType: string;
  public loadingData: boolean;
  public processed: number;

  constructor(
    private notify: NotifyService,
    private friend: FriendService,
    private transport: TransportService,
    private media: MediaService
  ) {
    this.bucket = []
    this.audience = []
    this.sendTo = []
    this.loadingData = false;
    this.circumference = 0;
    this.file = null;
    this.audienceType = 'friends';
    this.processed = 0;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.dataSwitch(data)
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private dataSwitch(data: any) {
    switch (data.type) {
      case 'fragment-done':
        this.fragmented(data.msg);
        break;
    }
  }

  viewControl(view: string) {
    this.data_type = view;
    if (view == 'audience' && this.audience.length == 0) {
      this.fetchFriends();
    }
  }

  onAudienceChange(event: any) {
    let value = event.target.value;
    this.audienceType = value;
    this.fetchFriends();
  }

  private fetchFriends() {
    this.loadingData = true;
    this.friend.get(this.audienceType).subscribe((data: any) => {
      data = data.users;
      this.audience = [];
      for (let i = 0; i < data.length; i++) {
        this.audience.push({
          name: data[i].name,
          email: data[i].email,
          image: (data[i].image != "") ? data[i].image : environment.basePath("../../../../assets/img/default.png"),
          share: data[i].share
        });
      }
      this.countAudience()
      this.loadingData = false
    });
  }

  private countAudience() {
    this.sendTo = []
    if (this.audience.length != 0) {
      this.audience.forEach((i: any) => {
        if (i.share) this.sendTo.push(i);
      })
    }
  }

  manageAudience(index: number, item: any, action: string) {
    let share = false;
    if (action == 'add') {
      share = true
    }
    this.audience[index].share = share
    this.countAudience()
  }

  /**
   * Pushing added media to the bucket object, one file at a time
   * @param event
   */
  addMedia(event: any) {
    let _file: File[] = event.target.files;
    if (_file.length == 0) return false;
    let extensionParts = _file[0].type.toLowerCase().split('/');
    console.log("Format:", extensionParts)
    if (!(['video', 'audio'].includes(extensionParts[0])) || extensionParts[1] == undefined) {
      this.notify.showNotification('info', "Only video file is supported");
      event.target.value = '';
      return;
    }

    if (!Helpers.EXTENTIONS.includes(extensionParts[1].toLowerCase())) {
      this.notify.showNotification('info', "File type is not supported");
      event.target.value = '';
      return;
    }

    let duplicate = this.checkDuplicates(_file[0]);
    if (duplicate) {
      console.log("File '" + _file[0].name + "' has already been added");
      this.notify.showNotification('info', "Selected file is already added");
      event.target.value = '';
      return;
    }

    this.file = _file[0];
    this.showFileReader()
    event.target.value = '';
  }

  private checkDuplicates(file: any): boolean {
    if (this.bucket.length == 0) return false;

    let duplicate = false;

    for (let i = 0; i < this.bucket.length; i++) {
      let item = this.bucket[i]
      if (item.size == file.size && item.title == file.name) {
        duplicate = true;
        break;
      }
    }

    return duplicate;
  }

  public showFileReader() {
    setTimeout(() => {
      this.fileReader.show();
    }, 100)
  }

  public hideFileReader() {
    this.fileReader.hide();
  }

  public hiddenFileReader() {
    this.updateText("")
  }

  processFileInfo() {
    let video = document.createElement('video'),
      _file = this.file,
      duration = 0,
      img = '', finished = false;

    video.preload = 'metadata';
    video.volume = 0;

    this.disableBtn()

    if (_file == undefined) {
      this.updateText("Please select a file...")
      setTimeout(() => {
        this.autoHide()
      }, 2500)
      return;
    }
    this.updateText("Processing file...")

    setTimeout(() => {
      video.onloadedmetadata = (e) => {
        this.updateText("Almost there, please wait...")
        video.currentTime = 12;
      }

      video.ontimeupdate = () => {
        duration = video.duration;
        let canvas = document.createElement('canvas');
        canvas.height = video.videoHeight;
        canvas.width = video.videoWidth;
        let ctx = canvas.getContext('2d');
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        img = canvas.toDataURL('image/jpeg', 5);
        window.URL.revokeObjectURL(video.src);
        video.removeAttribute('src')
        video = null
        console.log("Got image...")

        this.updateText("Finished processing file...")
        finished = true;
      }

      video.src = URL.createObjectURL(_file);
      video.onerror = (e) => {
        this.notify.showNotification('error', "Video file is corrupt");
      }
    }, 10)

    let wait = setInterval(() => {

      if (!finished) return;

      clearInterval(wait)
      setTimeout(() => {
        this.hideFileReader()
        setTimeout(() => {
          this.bucket.push({
            image: img,
            title: _file.name,
            size: _file.size,
            duration: duration,
            path: _file.path,
            label: "waiting",
            processed: false
          })

          setTimeout(() => {
            this.media.handleRequest({
              file: _file,
              index: (this.bucket.length - 1)
            })
            this.disableBtn()
          }, 100)
        }, 1000)
      }, 1500)
    }, 100)

  }

  private disableBtn() {
    let btn = document.getElementById('continue-btn')
    btn.setAttribute('disabled', 'true')
  }

  private updateText(msg: string) {
    let progressText = document.querySelector('.progress-text');
    if (progressText == null) return;
    progressText.innerHTML = msg;
  }

  fragmented(data: any) {
    if (this.bucket[data.index] != undefined) {
      this.bucket[data.index].path = data.path;
      this.bucket[data.index].processed = true;
      let elem = document.querySelector('.item-' + data.index + ' .item-image');
      elem.classList.remove('processing')
      this.countProcessedFiles()
    }
  }

  countProcessedFiles() {
    this.processed = 0;
    let size = this.bucket.length;
    for (let i = 0; i < size; i++) {
      if (this.bucket[i].processed) {
        this.processed++;
      }
    }

    let btn = document.getElementById('continue-btn')
    if (this.processed == 0 || this.processed != size) {
      btn.setAttribute('disabled', 'true')
    } else {
      btn.removeAttribute('disabled')
    }

  }

  /**
   * Looping circle waiting for all processes to finish
   */
  private updateUI(init?: boolean) {
    const circle: any = document.querySelector('.progress-ring__circle'),
      percentElem: any = document.querySelector('.percent');

    if (init != undefined) {
      const radius = circle.r.baseVal.value;
      this.circumference = radius * 2 * Math.PI;
      circle.style.strokeDasharray = `${this.circumference} ${this.circumference}`;
      circle.style.strokeDashoffset = this.circumference;
      percentElem.innerHTML = "";
      return;
    }

    // if (percent > 50) {
    //   this.updateText("Finalizing file processing...")
    // }

    let count = 0;
    // let loop = setInterval(() => {
    //   const offset = this.circumference - count / 100 * this.circumference;
    //   circle.style.strokeDashoffset = offset;
    //   count++;
    // }, 2)

  }

  private autoHide() {
    setTimeout(() => {
      this.hideFileReader()
    }, 500)
  }

  removeItem(index: number) {
    this.media.removeFile(this.bucket[index].path)
    this.bucket.splice(index, 1)
    this.processed--;
    this.countProcessedFiles()
  }

  // Create websocket room then send notification to the audience
  createBucket(event: any) {
    let targets = document.getElementsByClassName('final-step');
    for (let i = 0; i < targets.length; i++) {
      targets[i].setAttribute('disabled', 'true')
      targets[i].classList.add('disabled')
    }

    this.transport.send({
      msg: {
        media: this.bucket,
        audience: this.sendTo,
        audienceType: 'all'
      },
      type: 'notify-audience'
    })
  }

}
