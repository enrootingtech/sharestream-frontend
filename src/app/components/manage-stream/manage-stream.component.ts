import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { Router, Event, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: '.manage-stream',
  templateUrl: './manage-stream.component.html',
  styleUrls: ['./manage-stream.component.scss']
})
export class ManageStreamComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public componentType = '';
  public peerConnection = null
  public localChannel = null; // RTCDataChannel for the local (sender)
  public remoteChannel = null; // RTCDataChannel for the local (sender)
  public bucket: any = [];
  public fileReader: any;
  public connected: boolean = false;
  private session: any;
  public heading: string;

  constructor(
    private chatService: ChatService,
    private transport: TransportService,
    private zone: NgZone,
    private navigate: Router,
    private token: TokenService
  ) { }

  ngOnInit() {
    this.session = null;
    this.bucket = []
    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.transportHandle(data)
      })
    )

    this.updateHeader();
    this.eventListeners();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  private eventListeners() {
    document.addEventListener('keyup', (e: any) => {
      // "F12" key
      console.log(e.keyCode)
      if (e.keyCode == 8) {

      }
    });
  }

  private userInfo() {
    let parseJSON = JSON.parse(localStorage.getItem("shareUserInfo")),
      data = this.token.get('user');
    this.session = {
      id: parseJSON.id,
      name: data.first_name + " " + data.last_name,
      email: data.email
    }
  }

  private updateHeader(info?: any) {

    this.navigate.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        switch (event.url) {
          case "/stream/join":
            this.heading = 'Join Stream';
            break;
          case "/stream/start":
            this.heading = 'Start Stream';
            break;
          case '/stream/player':
            this.heading = 'Host: Kuhle Hanisi';
            this.componentType = 'player'
            let header = document.getElementById('main-header'),
              elem = document.createElement('span');
            elem.style.color = '#d82a2a';
            elem.innerHTML = "Closed";
            elem.setAttribute('id', 'channel-status')

            if (header != null) {
              let span = header.querySelector('span')
              if (span != null) span.remove()
              header.append(elem)
            }
            break;
          default:
            break;
        }
      }
    });
  }

  private transportHandle(data: any) {

    switch (data.type) {
      case 'roomId':
        this.userInfo()
        this.navigateToPlayer(data, this.session.id, 'local')
        break;
      case 'unable-to-join':
        this.btnState('activate')
        console.log("Unable to Join Room " + data.roomId);
        break;
      case 'added-to-room':
        this.navigateToPlayer(data, data.sessionId, 'remote')
        break;
      case 'notify-audience':
        this.notifyAudience(data)
        break;
      case 'py-start-signal':
        this.startConnection(data)
        break;
      case 'start-remote-peer':
        this.startRemoteConnection()
        break;
      case 'answer':
        this.answerFromRemote(data)
        break;
      case 'sendChannel':
        this.sendChannel(data);
        break;
      case 'join-request':
        this.joinRequest(data);
        break;
      case "peer-offer":
        this.handleOfferMsg(data)
        break;
      case "new-ice-candidate":
        this.handleNewICECandidateMsg(data)
        break;
      default:

        break;
    }
  }

  /**
  * Handling Peer Connection
  */

  createPeerConnection() {
    console.log("Setting up a RTCPeerConnection...");

    // Create an RTCPeerConnection which knows to use our chosen
    // STUN server.
    const configuration = {
      iceServers: [
        {
          urls: [
            'stun:stun.l.google.com:19302',
            // 'stun:stun.l.google.com:19302?transport=udp'
          ]
        },
        {
          urls: 'turn:numb.viagenie.ca',
          username: 'hanisikuhle@gmail.com',
          credential: '58453'
        }
      ]
    };

    this.peerConnection = new RTCPeerConnection(configuration);
    this.connected = true;

    // Set up event handlers for the ICE negotiation process.
    this.peerConnection.onicecandidate = (event: any) => this.handleICECandidateEvent(event);
    this.peerConnection.oniceconnectionstatechange = (event: any) => this.handleICEConnectionStateChangeEvent(event);
    this.peerConnection.onsignalingstatechange = (event: any) => this.handleSignalingStateChangeEvent(event);

  }

  handleICECandidateEvent(event: any) {
    if (event.candidate) {
      this.chatService.send({
        type: "new-ice-candidate",
        msg: {
          candidate: event.candidate
        }
      });
    }
  }

  // Collect new Ice candidates and store them in IndexedDb
  private handleNewICECandidateMsg(data: any) {

    if (this.peerConnection == null) return;

    this.peerConnection.addIceCandidate(new RTCIceCandidate(data.msg.candidate))
      .catch(error => {
        // this.handleCallError(error)
        console.log("handleNewICECandidateMsg: ", error)
      });
  }

  private btnState(state: string) {
    let joinBtns = document.getElementsByClassName('join-btn');
    for (let i = 0; i < joinBtns.length; i++) {
      let btn = joinBtns[i];
      if (state == 'activate') {
        btn.removeAttribute('disabled')
      } else {
        btn.setAttribute('disabled', 'true')
      }
    }
  }

  handleICEConnectionStateChangeEvent(event: any) {
    let state = this.peerConnection.iceConnectionState;

    if (state == 'closed' || state == 'failed' || state == 'disconnected') {

    }

    if (state == 'disconnected') {
      this.connected = false;
    } else if (state == 'checking') {

    } else if (state == 'connected') {
      this.connected = true;
      this.transport.send({
        type: 'ice-state',
        msg: {
          state: state
        }
      });
    }

    if (this.peerConnection == null) {
      console.log("connection is null")
    }

    console.log("handleICEConnectionStateChangeEvent: " + (this.peerConnection != null) ? this.peerConnection.iceConnectionState : 'Nothing')
  }

  handleSignalingStateChangeEvent(event: any) {
    console.log("handleSignalingStateChangeEvent: ", this.peerConnection.signalingState)
    let state = this.peerConnection.signalingState;
    switch (state) {
      case "closed":
        // this.endCall();
        this.connected = false;
        break;
      default:
        break;
    }

  }

  private navigateToPlayer(data: any, sessionId: number, view: string) {
    // Navigate to player for preparing media content
    console.log("New room id ", data)
    this.zone.run(() => {
      this.navigate.navigateByUrl('/stream/player', {
        state: {
          sessionId: sessionId,
          roomId: data.roomId,
          view: view
        }
      }).then(() => {
        console.log("Opened player")
        this.chatService.send({
          type: 'online-audience',
          msg: {
            roomId: data.roomId
          }
        })
      })
    });
  }

  // Create websocket room then send notification to the audience
  private notifyAudience(data: any) {
    this.bucket = data.msg.media;
    this.userInfo()

    let message = {
      msg: {
        bucket: data.msg.media,
        to: data.msg.audience,
        audience: data.msg.audienceType,
        from: {
          name: this.session.name + "'s",
          id: this.session.id,
          email: this.session.email
        }
      },
      type: 'notification'
    };
    console.log("Broadcast message: ", message)
    this.chatService.send(message)

  }

  private startConnection(data: any) {

    // Initiate peer connection
    this.createPeerConnection();
    // Create the data channel and establish its event listeners
    this.localChannel = this.peerConnection.createDataChannel("sendChannel");
    this.localChannel.binaryType = 'arraybuffer';
    this.localChannel.onopen = (event: any) => this.handleSendChannelStatusChange(event);
    this.localChannel.onclose = (event: any) => this.handleSendChannelStatusChange(event);
    this.localChannel.onerror = (event: any) => {
      console.error('Data sendChannel', event)
    }

    // Creating Offer
    this.peerConnection.createOffer().then((sdp: any) => {
      return this.peerConnection.setLocalDescription(sdp)
    })
      .then(() => {
        // Send SDP to remote media
        let sdp = this.peerConnection.localDescription //.sdp.replace('b=AS:30', 'b=AS:1638400');

        let message = {
          msg: {
            sdp: sdp,
            roomId: data.msg.roomId,
            type: 'initiate-peer'
          },
          type: 'room'
        };
        this.chatService.send(message)
      })
  }

  handleSendChannelStatusChange(event: any) {
    let readyState = event.target.readyState;
    console.log(`Send channel state is: ${readyState}`);
    let status = document.getElementById('channel-status');
    if (readyState === "open") {
      this.connected = true;
      status.style.color = "#0ac025"
    } else {
      this.connected = false;
      status.style.color = "#d82a2a"
      console.log("Closed DataChannel", event.target)
    }

    status.innerHTML = (this.connected) ? "Open" : "Closed";
  }

  private joinRequest(data: any) {
    this.btnState('')
    this.bucket = data.msg.bucket
    console.log("Remote Bucket: ", this.bucket)

    let message = {
      msg: {
        to: data.msg.to,
        roomId: data.msg.roomId,
        from: data.msg.from
      },
      type: 'stream-request'
    };
    console.log("Sending join request ...")
    this.chatService.send(message)

  }

  private startRemoteConnection() {
    setTimeout(() => {
      this.transport.send({
        type: 'sync-bucket',
        msg: {
          bucket: this.bucket
        }
      })
      this.createPeerConnection()
      this.peerConnection.ondatachannel = (event: any) => this.receiveChannelCallback(event);
    }, 1000)
  }

  handleOfferMsg(data: any) {
    console.log('Handling the offer...', data);

    this.peerConnection.setRemoteDescription(new RTCSessionDescription(data.msg.sdp))
      .then(() => {
        return this.peerConnection.createAnswer();
      })
      .then((answer: any) => {
        return this.peerConnection.setLocalDescription(answer);
      })
      .then(() => {
        let message = {
          msg: {
            sdp: this.peerConnection.localDescription,
            to: data.msg.from,
            type: 'answer'
          },
          type: 'room'
        };
        console.log("Sending answer packet back to the caller")
        this.chatService.send(message)
      })
      .catch(error => this.handleError(error));

  }

  // Respond to the "answer" message sent by the remote user
  private answerFromRemote(data: any) {
    console.log("Call recipient has accepted your offer")
    // Configure the remote description, which is the SDP payload
    // in our "answer" message.
    if (this.peerConnection != null) {
      this.peerConnection.setRemoteDescription(new RTCSessionDescription(data.msg.sdp))
        .then(() => {
          this.transport.send({
            type: 'localChannel',
            msg: {
              channel: this.localChannel
            }
          })
        }).catch(error => this.handleError(error));
    } else {
      console.log("No peerConnection found")
    }
  }

  // Describute channel
  sendChannel(data: any) {
    setTimeout(() => {
      this.transport.send({
        type: 'local-bucket',
        msg: {
          bucket: this.bucket
        }
      })
    }, 1000)
  }

  receiveChannelCallback(event: any) {
    this.remoteChannel = event.channel;
    this.remoteChannel.binaryType = 'arraybuffer';
    this.remoteChannel.onmessage = (e) => this.handleReceiveMessage(e);
    this.remoteChannel.onopen = (e) => this.handleReceiveChannelStatusChange(e);
    this.remoteChannel.onclose = (e) => this.handleReceiveChannelStatusChange(e);
  }

  handleReceiveMessage(event) {
    // Send signal to Player Component
    this.transport.send({
      type: 'streamMessage',
      msg: {
        event: event
      }
    })
  }

  // Handle status changes on the receiver's channel.

  handleReceiveChannelStatusChange(event) {
    console.log("Receive channel's status has changed to " +
      event.target.readyState);
  }

  // Close the connection, including data channels if they're open.
  // Also update the UI to reflect the disconnected status.

  disconnectPeers() {

    // Close the RTCDataChannels if they're open.

    this.remoteChannel.close();
    this.localChannel.close();

    // Close the RTCPeerConnections

    this.peerConnection.close();

    this.remoteChannel = null;
    this.localChannel = null;
    this.peerConnection = null;

    // Update user interface elements

  }

  handleError(error: any) {
    console.error("Error: ", error)
  }

}
