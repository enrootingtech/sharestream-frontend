import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { ChatService } from 'src/app/services/chat/chat.service.js';
import { Subscription } from 'rxjs';
import { StorageService } from 'src/app/services/storage/storage.service.js';
import { Bucket } from 'src/app/interface/bucket.js';
import { ContextMenuService } from 'src/app/utility/context-menu/context-menu.service.js';
import { MediacontrolService } from 'src/app/services/media-control/media-control.service.js';
import { UtilityService } from 'src/app/services/media-control/utility/utility.service.js';
import { Helpers } from 'src/app/utility/helpers.js';
import { TokenService } from 'src/app/services/token/token.service';
import { NotifyService } from 'src/app/services/notify/notify.service';

@Component({
  selector: '.player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public observe: Observable<any>;
  public bucket: Bucket[] = [];
  public transferedSize: number;
  public localChannel: any
  private mediaSource: any;
  private sourceBuffer: any;
  private chunkBuffer: any = [];
  private video: any;
  private transferComplete: boolean;
  private roomId: any;
  public activeVideo: number;
  public sessionId: number;
  public view: string;
  public inControl: boolean;
  public initialized: boolean;
  public movieStarted: boolean;
  public step: number;
  private savedIndexes: any;
  private isLast: boolean;
  private endWatch: boolean;
  public poster: string;
  public currentDuration: number;
  public endDuration: number;
  private syncCount: number;
  private indexCount: number;
  public currentVolume: number;
  public muteSound: boolean;
  public next: boolean;
  public prev: boolean;
  public audience: any;

  constructor(
    private transport: TransportService,
    private chatService: ChatService,
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private contextMenu: ContextMenuService,
    private mediaService: MediacontrolService,
    private playbackUtility: UtilityService,
    private token: TokenService,
    private notify: NotifyService
  ) {
    this.muteSound = false;
    this.currentVolume = 0.5;
  }

  ngOnInit() {

    this.video = null
    this.bucket = []
    this.chunkBuffer = [];
    this.activeVideo = -1;
    this.sessionId = 0
    this.endDuration = 0
    this.savedIndexes = [];
    this.audience = [];
    this.isLast = false;
    this.inControl = false;
    this.endWatch = false;
    this.currentDuration = 0
    this.transferComplete = false;
    this.step = 0;
    this.initialized = false;
    this.poster = ''
    this.transferedSize = 0
    this.movieStarted = false;
    this.mediaSource = null;
    this.sourceBuffer = null;
    this.syncCount = 0;
    this.indexCount = 0;
    this.next = false;
    this.prev = false;

    this.observe = this.activatedRoute.paramMap.pipe(
      map(() => window.history.state))

    this.subscriptions.push(
      this.observe.subscribe((data: any) => {
        if (data.roomId != undefined) {

          this.sessionId = data.sessionId;
          this.roomId = data.roomId;
          this.view = data.view;
          this.inControl = false;
          if (data.view == 'local') {
            this.transport.send({
              type: 'sendChannel',
              msg: []
            })
            this.inControl = true;
          } else if (data.view == 'remote') {
            this.transport.send({
              type: 'start-remote-peer',
              msg: []
            })
          }
        }
      })
    )

    this.subscriptions.push(
      this.transport.call.subscribe((data: any) => {
        this.dataSwitch(data)
      })
    )

    // this.contextMenu.activate();
  }

  ngOnDestroy() {

    let localData = this.token.get('user');
    if (localData != null) {
      this.chatService.send({
        type: "kill-stream",
        msg: {
          email: localData.email,
          roomId: this.roomId
        }
      });
    }

    this.subscriptions.forEach((subscription) => subscription.unsubscribe());

    this.sourceBuffer = null;
    if (this.mediaSource != undefined && this.mediaSource.readyState == 'open') {
      this.mediaSource.endOfStream();
      this.mediaSource = null;
    }

    let video = document.getElementById('stream-src')
    if (video != null) {
      video.remove()
    }

    window.indexedDB.deleteDatabase(this.roomId + "_movie");
  }

  dataSwitch(data: any) {

    switch (data.type) {
      case 'streamMessage':
        this.onReceiveMessage(data)
        break;
      case 'local-bucket':
        this.receivedBucketSync(data)
        break;
      case 'localChannel':
        this.receivedChannel(data)
        break;
      case 'sync-bucket':
        this.bucket = data.msg.bucket;
        break;
      case 'movie-label':
        this.syncBucket(data)
        break
      case 'ice-state':
        this.connectionState(data);
        break;
      case 'chunkSaved':
        this.chunkSaved(data.msg)
        break;
      case 'sendSync':
        this.sendSync(data)
        break;
      case 'play-control':
        this.remoteControl(data.msg.play);
        break;
      case 'end-of-playback':
        this.endRemotePlayback();
        break;
      case 'playback':
        this.playback(data.msg)
        break;
      case 'transcoding-complete':
        this.isLast = true;
        break;
      case 'sync-count':
        this.syncCount = data.msg.count;
        break;
      case 'kill-stream':
        this.killStream(data);
        break;
      case 'online-audience':
        this.onlineAudience(data.msg);
        break;
      case 'cleanup-signal':
        this.cleanupSignal();
        break;
      default:
        break;
    }

  }

  private onlineAudience(data: any) {
    let _thisUser = this.token.get('user');
    let audience = data;
    this.audience = [];

    // Add the logged in user first
    for (let i = 0; i < audience.length; i++) {
      if (audience[i].email == _thisUser.email) {
        this.audience.push(audience[i])
        break;
      }
    }

    // the add admins
    for (let i = 0; i < audience.length; i++) {
      if (audience[i].admin && audience[i].email != _thisUser.email) {
        this.audience.push(audience[i])
      }
    }

    // finally add other users
    for (let k = 1; k < audience.length; k++) {
      if (!audience[k].admin && _thisUser.email != audience[k].email) {
        this.audience.push(audience[k])
      }
    }
    console.log('Online audience: ', this.audience)
  }

  private killStream(data: any) {
    console.log("Kill stream signal: ", data)
  }

  public contact(id: number, email: string) {
    console.log("Details: id = " + id + ", email = " + email)
  }

  public activate(index: number, label: string) {
    let stage = label,
      nextLabel = '';

    if (stage == 'ready' || stage == 'preparing' || !this.inControl) {
      return;
    }

    if (this.audience.length <= 1) {
      this.notify.showNotification('info', "Need an audience to start")
      // return;
    }

    // if(this.initialized) this.initialized = false;

    if (stage == 'waiting') {
      nextLabel = 'preparing';
    }

    if (this.activeVideo != -1) {
      this.transport.send({
        type: 'cleanup',
        msg: []
      })
      this.savedIndexes = []
      if (this.video != null) {
        this.video.pause();
      }
    } else {
      // Initialize database
      let info = {
        items: this.bucket.length,
        room: this.roomId,
        type: 'movie'
      }
      console.log("Initializing Info: ", info)
      this.storageService.init(info)
    }

    this.activeVideo = index;

    if (this.bucket[this.activeVideo + 1] != undefined) {
      this.next = true;
    } else {
      this.next = false;
    }

    if (this.bucket[this.activeVideo - 1] != undefined) {
      this.prev = true;
    } else {
      this.prev = false;
    }

    this.endDuration = this.bucket[index].duration;

    this.changeVideoLabel(index, nextLabel)

    // Put other videos on Waiting state
    this.resetOtherVideo(index)

    this.bucket[index].label = nextLabel;
    let target = this.bucket[index];
    if (nextLabel == 'preparing') {
      this.initMediaProcessor(target)
    } else if (nextLabel == 'ready') {
      console.log('To ready stage: ', target)
    }

  }

  public nextMedia(index: number) {
    this.activate(index, 'waiting')
  }

  public prevMedia(index: number) {
    this.activate(index, 'waiting')
  }

  private cleanupSignal() {
    this.currentDuration = 0
    this.playbackUtility.updateUI(0, 'watch-track')
    this.playbackUtility.updateUI(0, 'buffer')
  }

  private resetOtherVideo(index: number) {

    this.chunkBuffer = []
    this.mediaSource = null;
    this.sourceBuffer = null;
    this.playback('pause')

    for (let i = 0; i < this.bucket.length; i++) {
      if (i !== index) {
        this.bucket[i].label = "waiting"
      }
    }
  }

  private changeVideoLabel(index, nextLabel) {
    let message = {
      msg: {
        roomId: this.roomId,
        type: 'movie-label',
        movie: index,
        label: nextLabel
      },
      type: 'room'
    };
    this.chatService.send(message)
  }

  // Receive DataChannel

  private receivedChannel(data: any) {
    this.localChannel = data.msg.channel
    console.log('Received DataChannel..', this.localChannel);
  }

  private initMediaProcessor(data: any) {

    console.log('To preparing stage: ', data)
    this.step = 1;
    this.movieStarted = false;
    this.poster = data.image

    let playerData = {
      file: {
        duration: data.duration,
        size: data.size,
        path: data.path
      },
      inControl: this.inControl,
      activeVideo: this.activeVideo,
      roomId: this.roomId
    }

    this.mediaService.initiate(playerData)

    let stateWatch = setInterval(() => {
      if (this.initialized) {
        clearInterval(stateWatch)
        let nextLabel = 'ready'
        this.videoEvents();
        this.changeVideoLabel(this.activeVideo, nextLabel)
        this.bucket[this.activeVideo].label = nextLabel;
      }

    })

  }

  private changeSteps(step: string) {

    let steps: any = document.getElementsByClassName('step');
    for (let i = 0; i < steps.length; i++) {
      steps[i].classList.add('d-none');
    }

    let elem = document.getElementsByClassName('step-' + step)[0];
    elem.classList.remove('d-none');
  }

  private initPlay(step: string) {
    let stepText: any;
    let container = document.getElementsByClassName('player-contain')[0];
    if (step == 'two') {
      let playerBox = document.getElementById('player-box');
      if (playerBox == null) return;

      playerBox.classList.add('auto-layer');
      stepText = document.querySelector('.step-two .step-text');
      container.classList.add('started')
      stepText.classList.add('d-none');
    } else {
      stepText = document.querySelector('.step-one .step-text');
      if (stepText == null) return
      stepText.innerHTML = "Stream is about to start"
    }

  }

  playback(state?: string) {

    if (!this.initialized) {
      if (this.inControl) console.log("Please select video you want to play");
      return 0;
    }

    if (this.step != 2 || !this.inControl) {
      return;
    }

    let mainPlayIcon = document.querySelector('.control-btn .fa'),
      mediaPlay = document.querySelector('#media-control-play .fa'),
      playing = false,
      video: any = document.getElementById('stream-src'),
      forcePause = () => {
        playing = false;
        mainPlayIcon.classList.remove('fa-pause');
        mediaPlay.classList.remove('fa-pause')
        mainPlayIcon.classList.add('fa-play');
        mediaPlay.classList.add('fa-play')
      };

    if (video.ended) {
      forcePause()
      return;
    }

    // this.videoEvents()

    if (state == undefined) {
      if (video.paused) {
        playing = true;
        mainPlayIcon.classList.remove('fa-play');
        mediaPlay.classList.remove('fa-play');
        mainPlayIcon.classList.add('fa-pause');
        mediaPlay.classList.add('fa-pause');
      } else {
        forcePause()
      }
    } else {
      forcePause()
    }


    let initPlay = () => {
      this.initPlay('two');
      this.movieStarted = true;
      video.style.opacity = 1;
    }

    let delay = 1500;
    if (this.movieStarted) {
      delay = 1;
    }

    setTimeout(() => {
      let play = false;
      if (playing) {
        play = true;

        if (!this.movieStarted) {
          initPlay()
        }
      }
      let msg = {
        msg: {
          roomId: this.roomId,
          type: 'play-control',
          play: play
        },
        type: 'room'
      }
      this.chatService.send(msg)

      setTimeout(() => {
        if (play) {
          video.play();
        } else {
          video.pause();
        }
      }, 1)

      console.log("Send remote control", msg)

    }, delay);

  }

  volume(event: any) {
    let val = parseFloat(event.target.value),
      video: any = document.getElementById('stream-src');
    this.currentVolume = val;
    if (this.currentVolume != 0) {
      this.muteSound = false;
    } else {
      this.muteSound = true;
    }
    if (video != null) {
      video.volume = this.currentVolume
      this.changeVolume('volume')
    }

  }

  mute() {

    let volume: any = document.getElementById('volume'),
      video: any = document.getElementById('stream-src');

    if (video == null || this.currentVolume <= 0) return;
    if (!this.muteSound) {
      video.volume = 0;
      volume.value = 0;
      this.muteSound = true;
    } else {
      video.volume = this.currentVolume;
      volume.value = this.currentVolume;
      this.muteSound = false;
    }
    this.changeVolume('mute')

  }

  private changeVolume(command: string) {
    let volumeIcon = document.querySelector('.volume-icon i');
    let change = () => {
      if (this.currentVolume >= 0.1 && this.currentVolume <= 0.5) {
        volumeIcon.classList.add('fa-volume-down')
      } else if (this.currentVolume >= 0.6) {
        volumeIcon.classList.add('fa-volume-up')
      } else if (this.currentVolume <= 0) {
        volumeIcon.classList.add('fa-volume-off')
      }
    }
    volumeIcon.classList.remove('fa-volume-up')
    volumeIcon.classList.remove('fa-volume-off')
    volumeIcon.classList.remove('fa-volume-down')
    console.log(command, this.muteSound)
    if (command == 'mute' && this.muteSound) {
      volumeIcon.classList.add('fa-volume-off')
    } else {
      change()
    }

  }

  private timeEvent(video: any) {
    let currentTime = video.currentTime;

    setTimeout(() => {
      this.currentDuration = currentTime;
    }, 1)

    setTimeout(() => {
      this.playbackUtility.updateUI(100 * (this.currentDuration / this.endDuration), 'watch-track')
    }, 1)
    console.log("currentDuration: " + this.currentDuration + ", endDuration: " + this.endDuration)

    let isEnded = () => {
      this.endOfPlayback(video)
      setTimeout(() => {
        this.playback('pause')
      }, 1)

      setTimeout(() => {
        this.chatService.send({
          msg: {
            roomId: this.roomId,
            type: 'end-of-playback',
            play: 'pause'
          },
          type: 'room'
        }
        )
      }, 1)
    }
    if (this.currentDuration >= this.endDuration) {
      isEnded()
    }

  }

  private videoEvents() {
    console.log("Setting new events")
    let video: any = document.getElementById('stream-src');
    this.video = video;
    video.removeEventListener('timeupdate', () => this.timeEvent(video))
    video.addEventListener('timeupdate', () => this.timeEvent(video));

  }

  private endOfPlayback(video: any) {
    console.log("Video has ended")
    this.playbackUtility.updateUI(100, 'watch-track')
    let watch: any = document.getElementsByClassName('watch-track')[0];
    watch.style.width = '100%';
    video.style.opacity = 0;
    this.playbackUtility.resetContainer()
  }

  /**
   * Prepare for streaming
   */
  public sendStartSignal(event: any) {
    this.transport.send({
      type: 'py-start-signal',
      msg: {
        roomId: this.roomId
      }
    });
    let btn = event.target,
      text = document.getElementById('connect-text')
    btn.setAttribute('disabled', "true")
    btn.innerHTML = "Connecting"
    text.innerHTML = "Start sharing with your audience"
  }

  private connectionState(data: any) {
    let text = document.getElementById('connect-text'), btn: any,
      state = data.msg.state;
    console.log("State has changed: " + state)
    if (this.inControl) {
      btn = document.getElementById('connect')
    }

    if (text == null) return;

    if (state == 'closed' || state == 'failed' || state == 'disconnected') {
      if (this.inControl) {
        btn.innerHTML = 'Connect';
        text.innerHTML = "Unable to connect with audience, please try again";
      } else {
        text.innerHTML = "Reconnecting with host, please wait"
      }
      this.step = 1;
    } else if (state == 'connected') {

      if (this.inControl) {
        btn.innerHTML = 'Connected';
        setTimeout(() => {
          btn.innerHTML = 'Syncing playback';
          text.innerHTML = "Syncing with the audience";

          this.watchChunkBuffer()
        }, 1000)
      } else {
        text.innerHTML = "Successfully connected with host"
        setTimeout(() => {
          text.innerHTML = "Waiting to be in sync with host"
        }, 1000)
      }

    } else if (state == 'checking' || state == 'stable') {
      if (this.inControl) {
        text.innerHTML = "Making connection with the audience"
      } else {
        text.innerHTML = "Making connection with the host"
      }
      this.step = 1;
    }
  }

  private chunkSaved(info: any) {
    this.savedIndexes.push(info.movieId);

    if (!this.initialized) {
      this.initialized = true;
      this.syncPlayback(true)
    }

  }

  /**
   * Syncing with other peers by sending the first chunk stored in the database
   */
  private syncPlayback(init: boolean) {

    let saveWatch = setInterval(() => {

      if (this.localChannel == null) return;

      // console.log("indexCount: " + this.indexCount + ' == ' + this.syncCount + ', isLast: ' + this.isLast)
      if (this.localChannel.readyState != 'open') return;

      let index = this.savedIndexes.shift();
      let getInfo = {
        video: this.activeVideo,
        room: this.roomId,
        movieId: index,
        type: 'movie'
      }

      if (index != null) {
        this.storageService.get(getInfo)
      }

      if (this.isLast && this.savedIndexes.length == 0) {
        this.storageService.count(getInfo)
        if (this.indexCount == this.syncCount) {
          console.log("End of watch")
          clearInterval(saveWatch);
          this.endWatch = true;
        }

      }
    }, 100)

  }

  /**
   * Wait till datachannel is done sending...
   */
  private watchChunkBuffer() {
    this.transferComplete = true;
    let watch = setInterval(() => {

      if (this.transferComplete && this.chunkBuffer.length > 0) {
        let obj = this.chunkBuffer.shift();
        if (obj == null) return;

        let chunk = obj.chunk;

        if (this.localChannel.readyState == 'open') {
          this.transferComplete = false;
          if (chunk.byteLength > (1024 * 14)) {
            this.chunkPiece(chunk);
          } else {
            this.localChannel.send(chunk)
            this.indexCount++;
            this.transferComplete = true;
          }
        }
        setTimeout(() => {
          if (this.step == 1) {
            this.step = 2;
            if (this.inControl) {
              let btn = document.getElementById('connect')
              btn.innerHTML = 'Ready to share';
              setTimeout(() => {
                this.changeSteps('two')
              }, 1000)
            }
          }

        }, 100)
      }

      if (this.endWatch && !this.transferComplete && this.chunkBuffer.length > 0) {
        this.localChannel.send("end_of_data");
        console.log("End of chunkBuffer")
        clearInterval(watch);
      }

    }, 100)

  }

  /**
   * When chunk size is greater than 16kb, break it down into pieces that are <= 16kb
   * @param chunk
   */
  private chunkPiece(chunk: any) {

    let chunkTransfer = 0,
      chunkSize = 1024 * 14;

    let reader = (data, _chunkSize) => {

      let blob = data.slice(chunkTransfer, chunkTransfer + _chunkSize)
      chunkTransfer += blob.byteLength;
      this.localChannel.send(blob)

      if (chunkTransfer >= chunk.byteLength) {
        this.indexCount++;
        this.transferComplete = true;
        return;
      }

      reader(data, _chunkSize)
    }

    reader(chunk, chunkSize)
  }

  /**
   * Send chunk to the room of peers using datachannel
   */
  private sendSync(data: any) {
    if (this.localChannel == null) return;

    if (this.localChannel.readyState != 'open') {
      console.log("You are not connected with your peers")
      // this.playback('pause')
      return;
    }

    this.chunkBuffer.push({
      chunk: data.msg.chunk
    })

  }

  //=============== Remote Peer Methods ======================

  private endRemotePlayback() {
    this.endOfPlayback(this.video)
    this.remoteControl(false);
  }

  private syncBucket(data: any) {
    let msg = data.msg;
    if (this.bucket[msg.movie] == undefined) {
      console.log("Movie: " + msg.movie + " is not available");
      return;
    }
    if (this.activeVideo != -1) {

    }
    this.activeVideo = msg.movie;

    this.resetOtherVideo(msg.movie)
    this.bucket[msg.movie].label = msg.label;
    this.endDuration = this.bucket[msg.movie].duration;
    this.poster = this.bucket[msg.movie].image;

    if (msg.label == 'ready') {
      this.initRemoteMediaSource()
    }

    console.log("Movie Label: ", data.msg)
  }

  private initRemoteMediaSource() {

    let mime = Helpers.DEFAULT_CODEC.mp4;

    this.video = document.createElement('video');
    this.video.setAttribute('id', 'stream-src')
    this.video.style.width = "100%";
    this.video.style.height = "100%";
    this.video.style.objectFit = "contain";
    this.video.style.opacity = 0;
    this.movieStarted = false;

    this.mediaSource = new MediaSource();
    //create an URL (from mediaSource OBJ) as video's source
    this.video.src = window.URL.createObjectURL(this.mediaSource);

    let videoContainer = document.getElementsByClassName('player-contain')[0];

    if (videoContainer.childNodes[1] != undefined) {
      videoContainer.removeChild(videoContainer.childNodes[1])
    }
    videoContainer.append(this.video);

    //listen source's open
    this.mediaSource.addEventListener('sourceopen', (e) => {
      this.mediaSource.duration = 0
      if (MediaSource.isTypeSupported(mime)) {
        try {
          console.info("MSE - SourceBuffer Creation with type '" + mime + "'");
          this.sourceBuffer = this.mediaSource.addSourceBuffer(mime);
          this.sourceBuffer.addEventListener("error", function (e) {
            console.error("MSE SourceBuffer: ", e);
          });
          this.sourceBuffer.ms = this.mediaSource;
          this.sourceBuffer.pendingAppends = [];
        } catch (e) {
          console.error("MSE - SourceBuffer: Cannot create buffer with type '" + mime + "'" + e);
        }
      } else {
        console.warn("MSE", "MIME type '" + mime + "' not supported for creation of a SourceBuffer");
      }
    });
  }

  private remoteControl(state: boolean) {

    let video: any = this.video;

    if (!this.movieStarted) {
      let container = document.getElementById('player-box'),
        stepText = document.querySelector('.step-one');

      container.classList.add('auto-layer');
      stepText.innerHTML = "Stream has started enjoy!"
      stepText.classList.add('d-none')
      this.movieStarted = true;
      video.style.opacity = 1;
    }

    video.addEventListener('timeupdate', () => {
      let currentTime = video.currentTime;
      setTimeout(() => {
        this.currentDuration = currentTime;
        this.playbackUtility.updateUI(100 * (this.currentDuration / this.endDuration), 'watch-track')
      }, 1)
    })

    let play = state
    if (play) {
      video.play();
    } else {
      video.pause();
    }
    console.log("Remote control: play =" + play)
  }

  private receivedBucketSync(data: any) {
    this.bucket = data.msg.bucket;
    console.log('Received Bucket Sync..', this.bucket);
  }

  private buffered = () => {
    let bufferedPercent = this.video.buffered.length > 0 ? Math.ceil(this.video.buffered.end(0) / this.endDuration * 100) : 0;
    return Math.ceil(bufferedPercent);
  };

  private remoteChannelClose(event: any) {
    event.addEventListener('close', (e: any) => {
      console.log("Channel closed", e)
    })
  }

  onReceiveMessage(data: any) {
    let event = data.msg.event,
      transferedData = event.data;

    console.log("Event:", transferedData)
    if (this.transferedSize == 0) {
      this.initPlay('one');
      this.remoteChannelClose(event.target);
    }

    this.chunkBuffer.push(transferedData)

    let waitBuffer = (arrayBuffer: any) => {

      console.log("sourceBuffer: " + this.sourceBuffer.updating + " and mediaSource is " + this.mediaSource.readyState, "Data: ", arrayBuffer)
      this.sourceBuffer.appendBuffer(arrayBuffer);

      // console.log("Receive size: " + this.transferedSize)

      this.playbackUtility.updateUI(this.buffered(), 'buffer')
    }

    let interval = setInterval(() => {

      if (this.sourceBuffer == null) return;

      if (!this.sourceBuffer.updating && this.mediaSource.readyState == 'open' && this.chunkBuffer.length > 0) {
        let data = this.chunkBuffer.shift();
        if (data == null) return;

        console.log("chunkBuffer: " + this.chunkBuffer.length + ", Done: ", data.byteLength)
        if (data == 'end_of_data') {
          this.transferComplete = true;
          console.log("Transfer complete")
          this.playbackUtility.updateUI(100, 'buffer')
          clearInterval(interval)
          return;
        }
        waitBuffer(data);
      }
    }, 100);


  }

}
