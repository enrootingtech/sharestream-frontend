export interface Friend {
    name: string,
    email: string,
    image: string,
    share: boolean
}
