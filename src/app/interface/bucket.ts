export interface Bucket {
    image: any,
    title: string,
    size: number,
    duration: number,
    path: string,
    label: string
}