import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Helpers } from 'src/app/utility/helpers';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  private url = Helpers.Url;

  constructor(
    private http: HttpClient,
    private token: TokenService
  ) { }

  get(audience: string) {
    return this.http.get(this.url + "account/" + this.token.getUser('user_id') + "/friends", {
      headers: this.token.headers(),
      params: {
        audience: audience
      }
    })
  }
}
