import { Injectable } from '@angular/core';
import { WebsocketService } from "../websocket/websocket.service";
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SocketMessage } from '../../interface/socketMessage';

@Injectable({
  providedIn: 'root'
})

export class ChatService {

  public messages: Subject<any>;

  constructor(private wsService: WebsocketService) {
    if (localStorage.getItem('share-socket') == 'online') {
      this.connect();
    }
  }

  public connect() {
    this.messages = <Subject<any>>this.wsService.connect().pipe(
      map((response: any): any => {
        return response
      })
    )
  }

  public send(message: SocketMessage) {
    this.messages.next(message)
  }

  public close() {
    this.wsService.close()
    this.messages = null
  }
}
