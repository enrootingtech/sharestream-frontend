import { Injectable } from '@angular/core';
import { ipcRenderer, webFrame, remote } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ElectronService {

  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  path: typeof path;
  _window: any;
  _ffmpeg: any;
  // ffprobePath: any;

  constructor() {
    this._window = window;
    if (this.isElectron()) {
      this.ipcRenderer = this._window.require('electron').ipcRenderer;
      this.webFrame = this._window.require('electron').webFrame;
      this.remote = this._window.require('electron').remote;
      this.path = this._window.require('path');
      this.childProcess = this._window.require('child_process');
      this.fs = this._window.require('fs');

      var ffmpegPath = this._window.require('@ffmpeg-installer/ffmpeg').path;
      // this.ffprobePath = this._window.require('@ffprobe-installer/ffprobe').path;

      // this._ffmpeg = environment.production ? ffmpegPath.replace('app.asar', 'app.asar.unpacked') : ffmpegPath;
      this._ffmpeg = ffmpegPath;

    }

  }

  isElectron = () => {
    return window && this._window.process && this._window.process.type;
  }

  // ffprobe = (path): Promise<any> => {
  //   return new Promise((resolve, reject) => {
  //     path = this.path.resolve(path);
  //     // this.ffmpeg.ffprobe(path, (err, data) => {
  //     //   if (err) return reject(err)
  //     //   return resolve(data)
  //     // })
  //     const ffprobeCommand = '"' + this.ffprobePath + '" -of json -show_streams -show_format "' + path + '"';
  //     this.childProcess.exec(ffprobeCommand, (err, data, stderr) => {
  //       if (err) return reject(err)
  //       const metadata = JSON.parse(data);
  //       return resolve(metadata)
  //     })
  //   })
  // }
}
