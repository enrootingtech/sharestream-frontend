import { Injectable } from '@angular/core';
import * as path from 'path';
import * as fs from 'fs';
import { StorageService } from '../storage/storage.service';
import { TransportService } from 'src/app/utility/transport/transport.service';
import { UtilityService } from './utility/utility.service';
import { ElectronService } from '../electron/electron.service';
import { Helpers } from 'src/app/utility/helpers';
import { Subscription } from 'rxjs';
import { Router, Event, NavigationStart } from '@angular/router';
import { environment } from '../../../environments/environment';
import * as Mp4Frag from 'mp4frag';

@Injectable({
  providedIn: 'root'
})
export class MediacontrolService {

  private subscriptions: Subscription[] = [];
  private mediaSource: any;
  private video: any;
  private inControl: boolean;
  private file: any;
  private endDuration: number;
  private chunkSize: number;
  private chunkProgress: number;
  private activeVideo: number;
  private roomId: any;
  private trackStore: any;
  private _platform: string;
  ffmpeg: any;
  private mp4frag: any;
  processing: number;
  private bufferStore: any;
  private sourceBuffer: SourceBuffer;
  private endWatch: any;
  private updateEnd: boolean;
  private movieId: number;
  private cleaning: boolean


  constructor(
    private storageService: StorageService,
    private transport: TransportService,
    private playbackUtility: UtilityService,
    private electronService: ElectronService,
    private route: Router
  ) {
    this.cleaning = false;
  }

  private cleanUp() {

    console.log("Clean up")
    // this.cleaning = true
    if (this.ffmpeg != null) {
      this.killCommand()
    }

    if (this.endWatch != null) {
      this.endWatch.stop = true;
    }

    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    this.subscriptions = [];

  }

  private killCommand() {
    this.ffmpeg.kill();
    this.ffmpeg = null;
    this.mp4frag = null
  }

  private init() {
    this.routeChanges()

    if (this.ffmpeg != null) {
      this.ffmpeg.kill();
    }

    if (this.endWatch != null) {
      this.endWatch.stop = true;
    }
    this.trackStore = []
    this._platform = ""
    this.bufferStore = [];
    this.mediaSource = null;
    this.sourceBuffer = null;
    this.chunkProgress = 0
    this.endWatch = {
      stop: false,
      msg: ''
    };
    this.movieId = 1
    this.updateEnd = true;

  }

  private routeChanges() {
    this.subscriptions.push(
      this.route.events.subscribe((event: Event) => {
        if (event instanceof NavigationStart) {
          if (event.url != '/stream/player') {
            this.cleanUp();
          }
        }

      })
    );

    this.subscriptions.push(
      this.transport.call.subscribe((event: any) => {
        if (event.type == 'cleanup') {
          this.cleanUp();
        }
      })
    );

  }

  /**
   * Path to save files
   */
  private getPlatform() {
    this._platform = process.platform;
  }

  private setDefault(data: any) {

    this.init()

    if (data.environment != 'test') {
      this.video = document.createElement('video');
      this.video.setAttribute('id', 'stream-src')
      this.video.style.width = "100%";
      this.video.style.height = "100%";
      this.video.style.objectFit = "contain";
      this.video.style.opacity = 0;
    } else {
      this.video = data.video
    }

    this.file = data.file;
    this.inControl = data.inControl;
    this.endDuration = data.file.duration
    this.chunkSize = 1024 * 14; // 16Kbytes
    this.activeVideo = data.activeVideo;
    this.roomId = data.roomId;

  }

  /**
   * Initiates the Media Source Extension for Mp4 Playback
   */
  initiate(data: any) {

    this.setDefault(data)

    this.mediaSource = new MediaSource();
    //create an URL (from mediaSource OBJ) as video's source
    this.video.src = window.URL.createObjectURL(this.mediaSource);

    if (data.environment != 'test') {
      let videoContainer = document.getElementsByClassName('player-contain')[0];

      if (videoContainer.childNodes[1] != undefined) {
        videoContainer.removeChild(videoContainer.childNodes[1])
      }
      videoContainer.append(this.video);
    }

    //listen source's open
    this.mediaSource.addEventListener('sourceopen', (e) => {
      this.mediaSource.duration = 0
      if (this.inControl) {
        this.onMediaSourceOpen()
      }
    });

  }

  // Media Source Handlers

  onMediaSourceOpen() {
    // this.startProcess()
    this.readAndSendFile()
  }

  buffered = () => {
    let bufferedPercent = this.video.buffered.length > 0 ? Math.ceil(this.video.buffered.end(0) / this.endDuration * 100) : 0;
    return bufferedPercent;
  };

  private saveBuffer(chunk: any, percent: number, movieId: number) {
    let info = {
      video: this.activeVideo,
      room: this.roomId,
      type: 'movie',
      chunk: chunk,
      percent: percent,
      movieId: movieId
    }
    this.storageService.set(info)
  }

  private closeAppend() {
    let watched = 100 * (this.video.currentTime / this.endDuration)
    if ((this.buffered() - watched) >= 8) {
      return false;
    }
    return true;
  }

  /**
   * Append to sourceBuffer, only when is's not updating and mediaSource is open,
   * Also saving ArrayBuffer to database. To avoid making the sourceBuffer full, only append data when media is playing
   */
  private bufferWatch() {

    let watch = setInterval(() => {

      // console.log("Pending buffer: " + this.bufferStore.length, ', closeAppend: ' + this.closeAppend() + ', sourceBuffer: ' + this.sourceBuffer.updating + ', updateEnd: ' + this.updateEnd)

      if (this.cleaning) {
        console.log("Still cleaning up!!")
        return;
      }

      if (!this.closeAppend()) {
        console.log("Pause buffering...")
      }

      if (this.bufferStore.length > 0 && !this.sourceBuffer.updating && this.updateEnd && this.closeAppend()) {
        let data = this.bufferStore.shift();
        if (data == null) {
          console.log("Data is null")
          return;
        }
        this.updateEnd = false;
        this.sourceBuffer.appendBuffer(data)
        let percent = this.buffered();
        this.playbackUtility.updateUI(percent, 'buffer')

        this.saveBuffer(data, percent, this.movieId)
        console.log("movieId: " + this.movieId)
        this.movieId++;

      }

      if (this.mediaSource == null) {
        clearInterval(watch);
        return;
      }

      if (this.mediaSource.readyState == 'ended' || this.endWatch.stop) {
        if (this.sourceBuffer.updating) return;

        clearInterval(watch);

        if (this.endWatch.msg == 'end') {
          this.mediaSource.endOfStream()
          this.killCommand()
        } else {
          console.log("MediaSource has " + this.mediaSource.readyState)
        }

        return;
      }

    }, 100)
  }

  private addEvents() {
    // console.log(this.sourceBuffer, this.video)
    this.sourceBuffer.addEventListener('error', (e) => {
      console.error(e)
      this.ffmpeg.kill();
    })
    this.sourceBuffer.addEventListener('updateend', (e) => {
      this.updateEnd = true
    })
  }

  private startProcess() {

    let spawn = this.electronService.childProcess.spawn,
      scale = 640,
      fps = 24,
      counter = 0;

    let args = [
      /* log info to console */
      '-re',
      '-loglevel', 'quiet',
      '-stats',

      /* use hardware acceleration if available */
      '-hwaccel', 'auto',

      /* use an artificial video input */
      //'-re',
      '-i', this.file.path,

      /* set output flags */
      //'-an',
      '-c:a', 'aac',
      '-c:v', 'libx264',
      '-movflags', '+faststart+frag_keyframe+empty_moov+default_base_moof+omit_tfhd_offset',
      '-f', 'mp4',
      '-vf', `fps=${fps},scale=${scale}:-1,format=yuv420p`,
      '-profile:v', 'main',
      '-level', '3.1',
      '-crf', '25',
      '-metadata', 'title=test mp4',
      '-reset_timestamps', '1',
      '-frag_duration', '5000000',//make ffmpeg create segments that are 30 seconds duration
      '-min_frag_duration', '5000000',// 30000000 make ffmpeg create segments that are 30 seconds duration
      'pipe:1'
    ]

    this.mp4frag = new Mp4Frag();

    this.mp4frag.once('initialized', (data) => {
      // console.log("Chunk size = " + data.initialization.byteLength)
      let sourceBuffer = this.mediaSource.addSourceBuffer(data.mime);
      this.sourceBuffer = sourceBuffer;
      this.addEvents()

      // const writeStream = fs.createWriteStream(`${outputPath}/init.mp4`);
      // writeStream.end(data.initialization);
      this.onCommandData(data.initialization)
      this.bufferWatch()
    });

    this.mp4frag.on('segment', (data) => {
      // const writeStream = fs.createWriteStream(`${outputPath}/seg-${counter}.m4s`);
      // writeStream.end(data);
      this.onCommandData(data)
      counter++;
    });

    this.mp4frag.once('error', (data) => {
      //error is expected when ffmpeg exits without unpiping
      console.log('mp4frag error', data);
    });
    // console.log("Path: ", this.electronService._ffmpeg)
    this.ffmpeg = spawn(this.electronService._ffmpeg, args, { stdio: ['ignore', 'pipe', 'inherit'] });

    this.ffmpeg.once('error', (error) => {
      console.log('ffmpeg error', error);
      this.onCommandError(error)
    });

    this.ffmpeg.once('exit', (code, signal) => {
      console.log('=====> Exit: ', code, signal);
      // this.onCommandEnd()
      if (signal == "SIGTERM") {
        this.transport.send({
          type: 'cleanup-signal',
          msg: []
        })
      }
    });

    this.ffmpeg.stdio[1].pipe(this.mp4frag);

  }

  private readAndSendFile() {
    // Finally, read the file chunk by chunk, store the chunks and send them
    let readStream = this.electronService.fs.createReadStream(this.file.path, { highWaterMark: this.chunkSize })
    let sourceBuffer = this.mediaSource.addSourceBuffer(Helpers.DEFAULT_CODEC.mp4);
    this.sourceBuffer = sourceBuffer;
    this.addEvents()

    readStream.on('data', (chunk) => {
      // console.log('readStream: data', chunk.byteLength)
      // Ideally, we should send the chunks to the other peer here
      this.onCommandData(chunk)

     }).on('end', function () {
      console.log('readStream: end')
    })
  }

  onCommandProgress(progress: any) {
    this.playbackUtility.updateUI(progress, 'progress', this.activeVideo)
    // console.log("onCommandProgresse ====>", progress);
  }

  private isLastCommand() {
    this.transport.send({
      type: 'transcoding-complete',
      msg: 'complete'
    })
  }

  onCommandError(err) {

    if (this.cleaning) {
      this.cleaning = false;
    } else {
      this.endWatch = {
        stop: true,
        msg: 'error'
      };
    }

    console.log('An error occurred: ', err);
    // console.log('ffmpeg output:\n' + stdout);
    // console.log('ffmpeg stderr:\n' + stderr);
  }

  onCommandEnd() {
    this.endWatch = {
      stop: true,
      msg: 'end'
    };
    this.playbackUtility.updateUI(100, 'progress', this.activeVideo)
    this.cleaning = false;
    this.isLastCommand()
    console.log('Processing finished !');
  }

  onCommandData(chunk: any) {
    this.bufferStore.push(chunk);
    if(this.chunkProgress == 0){
      this.bufferWatch()
    }
    this.chunkProgress += chunk.byteLength
    let progress = (this.chunkProgress / this.file.size) * 100;
    this.onCommandProgress(progress)
  }

}
