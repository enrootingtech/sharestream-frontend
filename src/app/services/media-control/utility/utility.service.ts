import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() {
  }

  resetContainer() {
    let playerBox = document.getElementById('player-box'),
      playerContain: any = document.getElementsByClassName('player-contain')[0];

    if (playerBox.classList.contains('auto-layer')) {
      playerBox.classList.remove('auto-layer')
    }

    if (playerContain.classList.contains('started')) {
      playerContain.classList.remove('started')
    }

  }

  updateUI(percentage: number, dataType: string, extra?: any) {

    if (dataType == 'buffer') {
      let bufferElem: any = document.getElementsByClassName('buffer-track')[0];
      if (bufferElem == null) return;
      bufferElem.style.width = percentage + "%";
    } else if (dataType == 'watch-track') {
      // get percentage of currently watched / max duration * 100 to get currently played percentage
      let watchElem: any = document.getElementsByClassName('watch-track')[0];
      if (watchElem == null) return;
      watchElem.style.width = percentage + "%";
    } else if(dataType == 'progress'){
      let playlistItem: any = document.querySelector('.playlist-item-' + extra + ' .item-segmentation');
      if (playlistItem == null) return;

      playlistItem.style.width = percentage + "%";
    }

  }

}
