import { TestBed } from '@angular/core/testing';

import { MediacontrolService } from './media-control.service';

describe('MediacontrolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediacontrolService = TestBed.get(MediacontrolService);
    expect(service).toBeTruthy();
  });
});
