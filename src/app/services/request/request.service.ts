import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../token/token.service';
import { Helpers } from 'src/app/utility/helpers';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private url = Helpers.Url;
  constructor(
    private http: HttpClient,
    private token: TokenService
  ) { }

  get(url: string, params?: any) {
    if (params == undefined) {
      params = {};
    }
    return this.http.get(this.url + url, {
      headers: this.token.headers(),
      params: params
    })
  }

  post(url: string, data: any, params?: any) {
    if (params == undefined) {
      params = {};
    }
    return this.http.post(this.url + url, data, {
      headers: this.token.headers(),
      params: params
    })
  }

  put(url: string, data: any, params: any) {
    if (params == undefined) {
      params = {};
    }
    return this.http.put(this.url + url, data, {
      headers: this.token.headers(),
      params: params
    })
  }
}
