import { Injectable } from '@angular/core';
import { TransportService } from 'src/app/utility/transport/transport.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private dbName: string;
  private tbName: string;

  constructor(
    private transport: TransportService
  ) { }

  /**
   * Initialize indexedDb
   * @param info
   */
  init(info: any) {
    this.openDatabase(info, 'init')
  }
  /**
   * Store chunk to indexedDB
   * @param info object, with Room Id and Video Id currently playing and also chunk percentage, chuck of type Uint8Array
   */
  set(info: any) {
    this.openDatabase(info, 'set')
  }

  private openDatabase(data: any, request: string) {
    // console.log(data)
    if (!('indexedDB' in window)) {
      console.log('This browser doesn\'t support IndexedDB');
      return;
    }

    this.dbName = data.room + "_" + data.type;
    let openRequest = indexedDB.open(this.dbName);

    openRequest.onerror = (e: any) => {
      console.log("Database error: " + e.target.errorCode);
    };

    openRequest.onsuccess = (evt: any) => {
      // console.log("running onsuccess on " + request);

      if (request == 'init') return;
      const db = evt.target.result;

      this.tbName = data.type + "_" + data.video;
      const transaction = db.transaction(
        this.tbName,
        (request == 'set') ? "readwrite" : 'readonly'
      );

      this.transactDb(transaction, data, request)

    };

    openRequest.onupgradeneeded = (evt: any) => {
      const db = evt.target.result;

      if (data.type == 'movie') {

        for (let i = 0; i < data.items; i++) {
          const movieStore = db.createObjectStore(
            "movie_" + i,
            { keyPath: "movieId", autoIncrement: true }
          );
          movieStore.createIndex("movieIndex", "movieId")
        }
      }
    };
  }

  private transactDb(transaction: any, data: any, request: string) {
    const movies = transaction.objectStore(this.tbName);

    if (request == 'set') {
      movies.add(
        { section: data.percent, chunk: data.chunk }
      );
      this.transport.send({
        type: 'chunkSaved',
        msg: {
          movieId: data.movieId
        }
      })
      console.log("Chunk added ")
    } else if (request == 'count') {
      let objectStore = transaction.objectStore(this.tbName);
      let requestCursor = objectStore.openCursor();
      let count = 0;
      requestCursor.onsuccess = (event) => {
        let cursor = event.target.result;
        if (cursor) {
          count++;
          cursor.continue();
        } else {
          // console.log('total number records saved: ' + count);
          this.transport.send({
            type: 'sync-count',
            msg: {
              count: count
            }
          })
        }
      };
    } else {

      let getRequest = movies.get(data.movieId)
      getRequest.onsuccess = (e) => {
        let result = e.target.result;
        // console.log("Get chunk: ", result)
        if (result == undefined) return;

        this.transport.send({
          type: 'sendSync',
          msg: {
            chunk: result.chunk,
            percent: result.section
          }
        })
      }
    }
  }

  get(info: any) {
    this.openDatabase(info, 'get')
  }

  count(info: any) {
    this.openDatabase(info, 'count')
  }

}
