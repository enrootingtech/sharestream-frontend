import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../token/token.service';
import { RequestService } from '../request/request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public jwtHelper: JwtHelperService,
    public tokenService: TokenService,
    private http: RequestService
  ) { }

  public isAuthenticated(): boolean {
    const token = this.tokenService.get('token');
    // Check whether the token is expired and return
    // true or false
    if (token == null || token == 'undefined') {
      return false;
    }
    return !this.jwtHelper.isTokenExpired(token);
  }

  login(data: any) {
    return this.http.post('auth/login', data);
  }

  register(data: any) {
    return this.http.post('auth/register', data);
  }

  logout() {
    return this.http.get('auth/logout');
  }

}
