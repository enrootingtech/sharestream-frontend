import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  private token_name = 'share_token';
  private token_user = 'share_user';

  handle(data) {
    if(data.user.picture == null || data.user.picture == ""){
      data.user.picture = environment.basePath('../../../assets/img/default.png')
    }
    this.set(this.token_user, JSON.stringify(data.user));
    this.set(this.token_name, data.access_token);
  }

  set(data_name, data_value) {
    localStorage.setItem(data_name, data_value)
  }

  removeAll() {
    let userSocketID = localStorage.getItem('shareUserInfo');
    localStorage.clear();
    localStorage.setItem('shareUserInfo', userSocketID);
  }

  remove(key: string) {
    localStorage.removeItem(key)
  }

  get(type: string) {
    let data: any;
    switch (type) {
      case 'user':
        data = JSON.parse(localStorage.getItem(this.token_user));
        break;
      case 'token':
        data = localStorage.getItem(this.token_name);
        break;
    }
    return data;
  }

  getUser(item?: string) {
    let data: any,
      parseJSON = JSON.parse(localStorage.getItem(this.token_user));
    switch (item) {
      case 'user_id':
        data = parseJSON.identity;
        break;
      default:
        data = parseJSON;
        break;
    }
    return data;
  }

  setUser(data) {
    return localStorage.setItem(this.token_user, JSON.stringify(data));
  }

  headers() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.get('token')
    })
  }

}
