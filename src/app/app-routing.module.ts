import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  AuthGuardService as AuthGuard
} from './services/auth/auth-guard/auth-guard.service';

import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ManageStreamComponent } from './components/manage-stream/manage-stream.component';
import { StartStreamComponent } from './components/manage-stream/start-stream/start-stream.component';
import { JoinStreamComponent } from './components/manage-stream/join-stream/join-stream.component';
import { PlayerComponent } from './components/manage-stream/player/player.component';
import { TestComponent } from './components/test/test.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    children: [
      { path: '', component: LoginComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent }
    ]
  },
  { path: 'test', component: TestComponent, canActivate: [AuthGuard] },
  {
    path: 'home',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  { path: 'stream', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'stream',
    component: ManageStreamComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'start', component: StartStreamComponent },
      { path: 'join', component: JoinStreamComponent },
      { path: 'player', component: PlayerComponent }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
