import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { ManageStreamComponent } from './components/manage-stream/manage-stream.component';
import { StartStreamComponent } from './components/manage-stream/start-stream/start-stream.component';
import { JoinStreamComponent } from './components/manage-stream/join-stream/join-stream.component';
import { PlayerComponent } from './components/manage-stream/player/player.component';
import { HelperPipe } from './utility/helper/helper.pipe';
import { TestComponent } from './components/test/test.component';
import { TopBarComponent } from './components/common/top-bar/top-bar.component';
import { ElectronService } from './services/electron/electron.service';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { WebviewDirective } from './directives/webview/webview.directive';

const notifierOption: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

export function jwtOptionsFactory() {
  return {
    tokenGetter: () => {
      return (localStorage.getItem('share_token') != 'undefined') ? localStorage.getItem('share_token') : null;
    },
    skipWhenExpired: true
  }
}

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    ManageStreamComponent,
    StartStreamComponent,
    JoinStreamComponent,
    PlayerComponent,
    HelperPipe,
    TestComponent,
    TopBarComponent,
    WebviewDirective,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    NotifierModule.withConfig(notifierOption),
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
      }
    })
  ],
  providers: [
    HelperPipe,
    ElectronService
  ],
  entryComponents: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
