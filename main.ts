import { app, BrowserWindow, screen, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';

let win: BrowserWindow, serve;
const args = process.argv.slice(1);
let currentWidth = 0, currentHeight = 0, positionY = 0, positionX = 0;
serve = args.some(val => val === '--serve');

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'

// var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
//   // Someone tried to run a second instance, we should focus our window.
//   if (win) {
//     if (win.isMinimized()) win.restore();
//     win.focus();
//   }
// });

// if (shouldQuit) {
//   app.quit();
//   process.exit();
// }


function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;
  const iconUrl = (serve) ? 'src' : 'dist';
  // Create the browser window.
  console.log(path.join('./', iconUrl + '/favicon.ico'), path.join(__dirname, iconUrl + '/favicon.ico'))
  win = new BrowserWindow({
    center: true,
    width: size.width - 100,
    height: size.height - 100,
    minWidth: 980,
    minHeight: 648,
    backgroundColor: "#f7f7f7",
    frame: false,
    icon: "file:///" + path.join(__dirname, iconUrl + '/favicon.ico'),
    webPreferences: {
      nodeIntegration: true,
      webviewTag: true
    }
  });

  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200/');
    win.webContents.openDevTools();
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    win.webContents.openDevTools();
  }



  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});


ipcMain.on("minimize", () => {
  if (win !== null) {
    win.minimize()
  }
})
ipcMain.on("fullscreen", () => {
  if (win !== null) {
    win.setFullScreen(!win.isFullScreen())
  }
})
ipcMain.on("maximize", () => {
  if (win !== null) {
    const electronScreen = screen;
    const size = electronScreen.getPrimaryDisplay().workAreaSize;
    const sizes = win.getSize();
    const pos = win.getPosition();
    positionY = pos[1]
    positionX = pos[0]
    currentWidth = sizes[0];
    currentHeight = sizes[1];
    win.setSize(size.width, size.height, true)
    win.center();
  }
})
ipcMain.on("restore", () => {
  if (win !== null) {
    win.setSize(currentWidth, currentHeight, true)
    win.setPosition(positionX, positionY)
  }
})
ipcMain.on("close", () => {
  if (win !== null) {
    win.close()
  }
})