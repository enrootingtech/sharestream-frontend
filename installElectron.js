const { series } = require("async");
const { exec } = require("child_process");

const _check = exec("npm list -g --depth 0");

_check.stdout.addListener("data", r => {
  if (!r.includes("electron")) {
    exec("npm i -g electron", (er, stdout, stderr) => {
      console.group("Electron:");
      console.log("Error: ", er);
      console.log("Stdout: ", stdout);
      console.log("Stderr: ", stderr);
      console.groupEnd();
    });
  }
});
